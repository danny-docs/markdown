# Readme

This repository contains file system documentation for various things - each directory has it's own url (/{directory}). If I were to use Parsedown Extra [^1] then I could have footnotes!

[^1]: [https://michelf.ca/projects/php-markdown/extra/#footnotes](https://michelf.ca/projects/php-markdown/extra/#footnotes)

