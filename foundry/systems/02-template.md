The template file ([reference](https://foundryvtt.com/article/system-development/#data)) defines the data structures used by actors and items.  Actors in foundry are anything with a character sheet (characters, NPC's, vehicles etc) and a token, whilst objects have just a stat sheet (equipment, armour, weapons etc, it's also not uncommon to represent skills or feats using items to allow for more variety).

Foundry allows you to create 'templates' of attributes that can then be applied to multiple different types (for instance you may defines 'characteristics' as a template that is used by 'NPC' and 'Player'), alternatively you can just define the characteristics directly on the object.

```json
{
    "Actor": {
        "types": [],
        "templates": {}
    },
    "Item": {
        "types": [],
        "templates": {}
    }
}
```

