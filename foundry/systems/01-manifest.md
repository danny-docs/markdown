The manifest file ([reference](https://foundryvtt.com/article/system-development/#manifest)) defines the basic settings for your game system (initiative, resource bars etc), the assets used for the system (js files, compendium packs) and meta data about the system(name, description, version etc).

For our system we are going to include `assets/modules/main.js' as an es6Module and 'assets/styles/main.css' as the stylesheet. We will also define HP and Mana as our default bar attributes.

==Warning: The 'name' field MUST match the name of the directory the system is installed into.==

```json
{
    "name": "demo-system",
    "title": "Demo system",
    "description": "",
    "version": "0.0.1",
    "templateVersion": 2,
    "minimumCoreVersion": "0.7.9",
    "compatibleCoreVersion": "0.7.9",
    "author": "Danny Cain <danny@dannycain.com>",
    "scripts": [],
    "esmodules": [
        "assets/modules/main.js"
    ],
    "styles": [
        "assets/styles/main.css"
    ],
    "languages": [],
    "packs": [],
    "primaryTokenAttribute": null,
    "secondaryTokenAttribute": null,
    "socket": false,
    "initiative": "2d6",
    "gridDistance": 1,
    "gridUnits": "m",
    "url": "https://your/hosted/system/repo/",
    "manifest": "https://your/hosted/system/repo/system.json",
    "download": "https://your/packaged/download/archive.zip"
}
```

