There are two types of entity that you are most likely to need to deal with:

Actors are things that have both a stat sheet and a token (e.g. characters, npc's, vehicles, monsters, etc).  

Items are things that have JUST a stat sheet and can belong to an Actor (e.g. weapons, armour, equipment), they can also be used to reflect more abstract things that can belong to an Actor (e.g. Skills, Feats).

An entity consists of a data template (in templates.json), a class (extending either Actor or Item), a sheet class (extending either ActorSheet or ItemSheet) and one or more handlebars templates.

The data template defines the default properties and values of an entity. The entity class allows defining derived attributes and adding helper methods.  The sheet class is responsible for selecting the correct template, providing data to the template and binding DOM event handlers.

