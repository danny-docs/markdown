The starting point for creating a system is the manifest file ([system.json](systems/manifest)) and the template file ([template.json](sytems/template)) - these files define the basic settings of the system and the data structures for actors and objects, you will also need to define an entry point for the initialisation of your system, I will be using assets/modules/main.js.

Once you have the stub of your module setup, you will want to move on to creating your entities.

