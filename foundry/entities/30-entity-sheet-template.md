Templates in Foundry are rendered using Handlebars - we'll cover this more in depth later, you will probably want at minimum an edit/view template and a 'limited' template. The edit/view template is used for owners and viewers (but the fields are disabled for view only), the limited template is used for those with 'limited' permissions on the entity and can be used to show just the description:

```html
{{!-- templates/actors/actor-sheet.html --}}
<form class="{{cssClass}}" autocomplete="off">
    <header class="sheet-header flexrow">
        <img class="profile" src="{{ actor.img }}" style="height: auto; max-width: 100px; " title="{{ actor.name }}" data-edit="img" />

        <section class="header-details flexrow">
            <h1 class="charname">
                <input name="name" type="text" value="{{actor.name}}" placeholder="{{ localize 'example.NAME' }}" />
            </h1>
        </section>
    </header>
    
    <nav class="sheet-navigation tabs mb-2" data-group="primary">
        <a class="item active" data-tab="tab-1">Tab 1</a>
        <a class="item" data-tab="tab-2">Tab 2</a>
    </nav>
    
    <section class="sheet-body mb-2">
        <div class="tab tab-1" data-group="primary" data-tab="tab-1">

            <div>St: <input type="number" class="small-number" name="data.St" value="{{ actor.data.St }}" /></div>
            <div>Dx: <input type="number" class="small-number" name="data.Dx" value="{{ actor.data.Dx }}" /></div>
            <div>Iq: <input type="number" class="small-number" name="data.Iq" value="{{ actor.data.Iq }}" /></div>
            <div>Ht: <input type="number" class="small-number" name="data.Ht" value="{{ actor.data.Ht }}" /></div>

            <div>HP: <input type="number" class="small-number" name="data.Hp.value" value="{{ actor.data.Hp.value }}" /> / {{ actor.data.Hp.max }}</div>
            <div>Mana: <input type="number" class="small-number" name="data.Mana.value" value="{{ actor.data.Mana.value }}" /> / {{ actor.data.Mana.max }}</div>
            </div>
        </div>

        <div class="tab tab-2" data-tab="tab-2">
            <div>
                Description:
                <textarea name="data.Description">{{ actor.data.Description }}</textarea>
            </div>
    
            <div>
                Background:    
                <textarea name="data.Background">{{ actor.data.Background }}</textarea>
            </div>
        </div>
    </section>
</form>
```

```html
{{!-- templates/actors/limited-actor-sheet.html --}}
<form class="{{cssClass}}" autocomplete="off">
    <header class="sheet-header flexrow">
        <img class="profile" src="{{ actor.img }}" style="height: auto; max-width: 100px; " title="{{ actor.name }}" />

        <section class="header-details flexrow">
            <h1 class="charname">
                {{ actor.name }}
            </h1>
        </section>
    </header>

    {{!-- Body --}}
    <section class="sheet-body mb-2">
        {{ actor.data.Description }}
    </section>
</form>
```

```html
{{!-- templates/items/item-sheet.html --}}
<form class="{{cssClass}}" autocomplete="off">
    <header class="sheet-header flexrow">
        <img class="profile" src="{{ item.img }}" style="height: auto; max-width: 100px; " title="{{ item.name }}" data-edit="img" />

        <section class="header-details flexrow">
            <h1 class="charname">
                <input name="name" type="text" value="{{item.name}}" placeholder="{{ localize 'example.NAME' }}" />
            </h1>
        </section>
    </header>

    <nav class="sheet-navigation tabs mb-2" data-group="primary">
        <a class="item active" data-tab="tab-1">Tab 1</a>
        <a class="item" data-tab="tab-2">Tab 2</a>
    </nav>

    <section class="sheet-body mb-2">
        <div class="tab tab-1" data-group="primary" data-tab="tab-1">
            Characteristic:
            {{!-- This uses the foundry select helper to select the current value --}}
            <select name="data.characteristic">
                {{#select item.data.Characteristic}}
                <option value=""></option>
                <option value="St">St</option>
                {{/select}}
            </select>

            Ranks:
            <input type="number" name="data.Ranks" value="{{ item.data.Ranks }}" />
        </div>

        <div class="tab tab-2" data-tab="tab-2">
			<div>
                Description:
            </div>
            <textarea name="data.Description">{{ item.data.Description }}</textarea>
        </div>
    </section>
</form>
```

```html
{{!-- templates/items/limited-item-sheet.html --}}
<form class="{{cssClass}}" autocomplete="off">
    <header class="sheet-header flexrow">
        <img class="profile" src="{{ item.img }}" style="height: auto; max-width: 100px; " title="{{ item.name }}" />

        <section class="header-details flexrow">
            <h1 class="charname">
                {{ item.name }}
            </h1>
        </section>
    </header>

    {{!-- Body --}}
    <section class="sheet-body mb-2">
        {{ item.data.Description }}
    </section>
</form>
```
