The first step in creating a new entity is to define the default data structure. This is done in template.json.

==Note: You must update the version number of your module/system in order for foundry to pick up the change==

## Define Type

Add the type name to the relevant types array (either Actor.types or Item.types)

## Define data structure

Define the data structure for the type under either Actor.{type} or Item.{type}. You can re-use data sets be using templates (defined as either Actor.templates.{name} or Item.templates.{name} and used with Actor.{type}.templates: [] or Item.{type}.templates: [])

## Example

Here we are defining two actors (Player and NPC), the only difference being that a player has a background.  We are also defining a single Item Skill to represent character's skills.

```json
// template.json
{
    "Actor": {
        "types": ["player", "npc"],
        "templates": {
            "characteristics": {
                "St": 10,
                "Dx": 10,
                "Iq": 10,
                "Ht": 10
            },
            "Hp": {
                "min": 0,
                "max": 10,
                "value": 10
            },
            "Mana": {
                "min": 0,
                "max": 10,
                "value": 10
            }
        },
        "player": {
            "templates": ["characteristics"],
            "Background": "",
            "Description": ""
        },
        "npc": {
            "templates": ["characteristics"],
            "Description": ""
        }
    },
    "Item": {
        "types": ["skill"],
        "skill": {
            "Characteristic": "St",
            "Ranks": 1,
            "Description" : ""
        }
    }
}
```

