The sheet class is used to select the template, prepare data for the template and bind DOM events.

```js
// assets/modules/sheets/character-sheet.js

export default class DemoCharacterSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/demo-system/templates/actors/limited-actor-sheet.html";
        }
        // use a full editable sheet
        return "systems/demo-system/templates/actors/actor-sheet.html";
    }
    
    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        return {
            actor: this.actor.data,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        if (!this.isEditable) {
            return;
        }

        super.activateListeners($doc);
    }
}
```

```js
// assets/modules/sheets/item-sheet.js

export default class DemoItemSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.object.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/demo-system/templates/items/limited-item-sheet.html";
        }
        // use a full editable sheet
        return "systems/demo-system/templates/items/item-sheet.html";
    }
    
    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        return {
            item: this.object.data,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        // bind any dom listeners that apply to all sheets
        if (!this.isEditable) {
            return;
        }

        // bind any dom listeners that apply to editable sheets
       
        // default bindings, including saving
        super.activateListeners($doc);
    }
}
```

