In your main.js you first need to unregister the core sheets and then register the sheets you wish to use:

```js
import DemoActor from "./entities/actor.js";
import DemoItem from "./entities/item.js";
import DemoCharacterSheet from "./sheets/character-sheet.js";
import DemoItemSheet from "./sheets/item-sheet.js";

Hooks.once('init', () => {
    // define entity classes
    CONFIG.Actor.entityClass = DemoActor;
    CONFIG.Item.entityClass = DemoItem;
    
    // unregister core sheets
    Actors.unregisterSheet("core", ActorSheet);
    Items.unregisterSheet("core", ItemSheet);
    
    // register custom sheets
    Actors.registerSheet("demo", DemoCharacterSheet, {
        types: ["player"],
        makeDefault: true,
        label: "PC",
    });
    
    Actors.registerSheet("demo", DemoCharacterSheet, 		{
        types: ["npc"],
        makeDefault: true,
        label: "NPC",
    });
    
    Items.registerSheet("demo", DemoItemSheet, {
        types: ["skill"],
        makeDefault: true,
        label: "Skill"
    });
});
```

