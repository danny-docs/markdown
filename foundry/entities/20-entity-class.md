Each entity is represented by a class specific to it's type.  This class handles all of the magic of the entity - dynamic attributes, custom methods etc. I define my entity classes in assets/modules/entities/*.js.

```js
// Actor class (assets/modules/entities/actor.js)

export default class DemoActor extends Actor
{
    // object setup - before items and effects
    prepareBaseData() {
        
    }
    
    // set any computed attributes for this entity, including from items and effects
    prepareDerivedData() {
        this.data.data.Hp.max = this.data.data.Ht;
        this.data.data.Mana.max = this.data.data.Iq;
    }
}

```

```js
// Item class (assets/modules/entities/item.js)

export default class DemoItem extends Item {
    // object setup - before items and effects
    prepareBaseData() {

    }

    // set any computed attributes, including from items and effects
    prepareDerivedData() {
        
    }
}
```

